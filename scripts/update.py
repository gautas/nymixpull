"""update the bigquery table with latest data
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
from nymixpull import mix
from nymixpull.google import google
from nymixpull.bucket import bucket
from nymixpull.table import dataset, staging, fuelmix
import time
import logging
import argparse

# %%
parser = argparse.ArgumentParser(description="Update the bigquery NYISO fuel mix table")

parser.add_argument(
    "--filepath_cred_gcs",
    type=str,
    help="path to google cloud storage credentials json",
    default="./keys/cred",
)

args = parser.parse_args()

# %%
logging.basicConfig(level=logging.INFO)

# %%
google.set_filepath_cred_gcs(args.filepath_cred_gcs)

# %%
# delete the staging table
staging.delete()

# %%
# create dataset and tables if do not exist
for obj in [dataset, staging, fuelmix]:
    if obj.exists is False:
        obj.create()

# %%
# download and extract the latest and next year/month zips

# %%
mixes = [
    mix.NYISOMixMonth.from_yearmonth(yearmonth)
    for yearmonth in [fuelmix.yearmonth_latest, fuelmix.yearmonth_next]
    if yearmonth.is_future is False
]

for month_mix in mixes:
    month_mix.download()
    time.sleep(10)

# %%
for month_mix in mixes:
    month_mix.extract()

# %%
# upload to bucket
for month_mix in mixes:
    month_mix.upload()

# %%
# add to staging table
for month_mix in mixes:
    month_mix.stage()

# %%
# add latest rows to fuelmix table
fuelmix.update()

# %%
# delete the staging table
staging.delete()
