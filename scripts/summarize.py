"""
generate summary csvs
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
import argparse
import logging
from nymixpull.google import google
from nymixpull.queries import Query

# %%
parser = argparse.ArgumentParser(description="Update the bigquery NYISO fuel mix table")

parser.add_argument(
    "--filepath_cred_gcs",
    type=str,
    help="path to google cloud storage credentials json",
    default="./keys/cred",
)

args = parser.parse_args()

# %%
logging.basicConfig(level=logging.INFO)

# %%
google.set_filepath_cred_gcs(args.filepath_cred_gcs)

# %%
for query in Query:
    query.save()
