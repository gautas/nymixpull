"""
update the postgres db table with new data
"""

import logging
from nymixpull.postgres import fuelmix, staging, update
from nymixpull.mix import NYISOMixMonth
import time

# set up the log file (also log to stdout)
FILE_HANDLER = logging.FileHandler(filename="logs/update_db.log")
STDOUT_HANDER = logging.StreamHandler()
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M",
    handlers=[FILE_HANDLER, STDOUT_HANDER],
)

# check if fuelmix table exist: if not, create it
if fuelmix.exists is False:
    fuelmix.create()

while True:

    # check if staging table exist: if so, delete it, and then create a fresh table
    if staging.exists is True:
        staging.delete()
    staging.create()


    # download the latest and next year/month zips
    mixes = [
        NYISOMixMonth.from_yearmonth(yearmonth)
        for yearmonth in [fuelmix.yearmonth_latest, fuelmix.yearmonth_next]
        if yearmonth.is_future is False
    ]

    for month_mix in mixes:
        month_mix.download()
        time.sleep(10)

    # extract the latest and next year/month zips
    for month_mix in mixes:
        month_mix.extract()

    # add to staging table
    for month_mix in mixes:
        month_mix.stage()

    # add latest rows to fuelmix table
    update()

    # delete the staging table
    staging.delete()

    # wait some sec
    sec = 30
    logging.info(f"sleeping for {sec} sec")
    time.sleep(sec)