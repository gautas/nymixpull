"""load a month's worth of NYISO fuel mix data to google cloud bucket
"""

from nymixpull import mix
from nymixpull.config import config, YEAR_START, MONTH_START
from nymixpull.bucket import bucket
from nymixpull.google import google
import time
import logging

logging.basicConfig(level=logging.INFO)

google.set_filepath_cred_gcs("./../keys/cred")

month = mix.NYISOMixMonth(year=YEAR_START, month=MONTH_START)

month.download()

month.extract()

for day in month.nyiso_mix_days:
    day.upload()
    time.sleep(10)