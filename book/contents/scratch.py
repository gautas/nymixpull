# %% [markdown]
"""
# Scratch
"""

# %% tags=['hide-cell']
from IPython import get_ipython
if get_ipython() is not None:
    get_ipython().run_line_magic('load_ext', 'autoreload')
    get_ipython().run_line_magic('autoreload', '2')


# %%
from datetime import datetime as dt
from datetime import date
from nymixpull import mix, table, google

# %%
mix = mix.NYISOMixMonth(year=2021, month=7)

# %%
mix_day = mix.nyiso_mix_days[0]

# %%
mix_day.blob

# %%
google.google.set_filepath_cred_gcs("./../../keys/cred")

# %%
client = table.Bigquery().client

# %%
client.project

# %%
table.dataset.create()

# %%
table.table.create()

# %%
