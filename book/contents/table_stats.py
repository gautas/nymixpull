# %% [markdown]
"""
# Table stats
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")

# %%
from nymixpull.queries import Query, config
import altair as alt
import os
import pandas as pd
from nymixpull.concepts import get_fuel_group_name

# %%
config.set_loc_result("./../../")

# %% [markdown]
"""
note to self: if running locally, set the env variable with `export CRED_GCS="/dockvol/nymixpull/keys/cred"`
"""


# %% [markdown]
"""
##  Latest timestamp
"""

# %%
pd.to_datetime(Query.STATS_UPDATE.read["CREATED"]).max().strftime(
    format="%Y-%m-%d %H:%M"
)

# %% [markdown]
"""
##  Number of rows
"""

# %%
f"{Query.STATS_UPDATE.read['NUM_ROWS'].sum():,.0f}"


# %% [markdown]
"""
##  Number of rows per month
"""

# %%
rows_per_month = Query.MONTHLY_TOTALS.read.groupby(
    ["YEAR", "MONTH"], as_index=False
).agg({"NUM_ROWS": "sum"})

# %%
rows_per_month.head()


# %%
alt.Chart(rows_per_month).mark_bar().encode(
    column="YEAR", x="MONTH:O", y="NUM_ROWS"
).properties(width=100)

# %% [markdown]
"""
##  Fuel types by year
"""
# %%
yearly_ttl_mw = (
    Query.MONTHLY_TOTALS.read.groupby(["YEAR"], as_index=False)
    .agg({"MW": "sum"})
    .rename(columns={"MW": "TTL_MW"})
)
fuel_types = (
    Query.MONTHLY_TOTALS.read.groupby(["YEAR", "FUEL"], as_index=False)
    .agg({"MW": "sum"})
    .merge(right=yearly_ttl_mw, on=["YEAR"], how="left", validate="many_to_one")
)

fuel_types["MW_PCNT"] = fuel_types["MW"] / fuel_types["TTL_MW"]

fuel_types.head()

# %%
alt.Chart(fuel_types).mark_line().encode(
    x="YEAR:N", y=alt.Y("MW_PCNT:Q", axis=alt.Axis(format="%")), color="FUEL"
)

# %%
fuel_types["FUEL_GROUP"] = fuel_types["FUEL"].map(get_fuel_group_name)

# %%
alt.Chart(fuel_types).mark_line().encode(
    x="YEAR:N", y=alt.Y("sum(MW_PCNT)", axis=alt.Axis(format="%")), color="FUEL_GROUP"
)

# %% [markdown]
"""
##  Fuel types by month
"""
# %%
monthly_ttl_mw = (
    Query.MONTHLY_TOTALS.read.groupby(["YEAR", "MONTH"], as_index=False)
    .agg({"MW": "sum"})
    .rename(columns={"MW": "TTL_MW"})
)

fuel_types_month = (
    Query.MONTHLY_TOTALS.read.groupby(["YEAR", "MONTH", "FUEL"], as_index=False)
    .agg({"MW": "sum"})
    .merge(
        right=monthly_ttl_mw, on=["YEAR", "MONTH"], how="left", validate="many_to_one"
    )
)

fuel_types_month["MW_PCNT"] = fuel_types_month["MW"] / fuel_types_month["TTL_MW"]

fuel_types_month.head()

# %%
alt.Chart(fuel_types_month).mark_line().encode(
    column="YEAR:N",
    x="MONTH:N",
    y=alt.Y("MW_PCNT:Q", axis=alt.Axis(format="%")),
    color="FUEL",
).properties(width=100)

# %%
fuel_types_month["FUEL_GROUP"] = fuel_types_month["FUEL"].map(get_fuel_group_name)

# %%
alt.Chart(fuel_types_month).mark_line().encode(
    column="YEAR:N",
    x="MONTH:N",
    y=alt.Y("sum(MW_PCNT)", axis=alt.Axis(format="%")),
    color="FUEL_GROUP",
).properties(width=100)

# %% [markdown]
"""
###  12-month rolling average
"""

# %%
fuel_types_month
# %%
fuel_types_month["ROLLING_MW"] = (
    fuel_types_month.sort_values(["YEAR", "MONTH"])
    .groupby(["FUEL"])["MW"]
    .transform(lambda s: s.rolling(12).mean())
)
# %%
alt.Chart(fuel_types_month).mark_line().encode(
    column="YEAR:N",
    x="MONTH:N",
    y="ROLLING_MW",
    color="FUEL",
).properties(width=100)

# %%
monthly_ttl_mw["ROLLING_TTL_MW"] = (
    monthly_ttl_mw.sort_values(["YEAR", "MONTH"])["TTL_MW"].rolling(12).mean()
)
# %%
monthly_ttl_mw
# %%
fuel_types_month_w_rolling_ttl = fuel_types_month.merge(
    right=monthly_ttl_mw[["YEAR", "MONTH", "ROLLING_TTL_MW"]],
    on=["YEAR", "MONTH"],
    how="left",
    validate="many_to_one",
)

# %%
fuel_types_month_w_rolling_ttl["ROLLING_MW_PCNT"] = (
    fuel_types_month_w_rolling_ttl["ROLLING_MW"]
    / fuel_types_month_w_rolling_ttl["ROLLING_TTL_MW"]
)
# %%
alt.Chart(fuel_types_month_w_rolling_ttl).mark_line().encode(
    column="YEAR:N",
    x="MONTH:N",
    y=alt.Y("ROLLING_MW_PCNT", axis=alt.Axis(format="%")),
    color="FUEL",
).properties(width=100)

# %%
alt.Chart(fuel_types_month_w_rolling_ttl).mark_line().encode(
    column="YEAR:N",
    x="MONTH:N",
    y=alt.Y("sum(ROLLING_MW_PCNT)", axis=alt.Axis(format="%")),
    color="FUEL_GROUP",
).properties(width=100)
# %%
