# %% [markdown]
"""
# Scrath
"""

# %%
import psycopg2
import os

# %%
conn = psycopg2.connect(
    host="db",
    database=os.environ.get("POSTGRES_DB"),
    user=os.environ.get("POSTGRES_USER"),
    password=os.environ.get("POSTGRES_PASSWORD"))
# %%
cur = conn.cursor()

# %%
cur.execute("SELECT version()")

# %%
cur.fetchone()

# %%
cur.close()

# %%
conn.close()
# %%
print(os.environ.get("POSTGRES_DB"))
# %%
