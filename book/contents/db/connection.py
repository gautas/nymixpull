# %% [markdown]
"""
# Connecting
How to use the tools in this package to connect to and execute on the postgres database
"""

# %% tags=['hide-cell']
from IPython import get_ipython

if get_ipython() is not None:
    get_ipython().run_line_magic("load_ext", "autoreload")
    get_ipython().run_line_magic("autoreload", "2")


# %%
from nymixpull.postgres import execute, fuelmix, staging
import logging
import sys

# %%
logging.basicConfig(
    stream=sys.stdout,
    level=logging.INFO,
    format="%(asctime)s %(levelname)s %(module)s - %(funcName)s: %(message)s",
    datefmt="%Y-%m-%d %H:%M",
)

# %%
res = execute("SELECT * FROM information_schema.tables")
# %%
res[0]
# %%
fuelmix.exists

# %%
staging.exists
# %%
execute("SELECT COUNT(*) FROM fuelmix LIMIT 1")
# %%
s = fuelmix.ttls_monthly

# %%
type(s)

# %%
s[0]
# %%
