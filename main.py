from fastapi import FastAPI
from nymixpull.postgres import fuelmix

app = FastAPI()


@app.get("/")
async def timestamp_latest():
    return fuelmix.timestamp_latest

@app.get("/created")
async def stats_created():
    return fuelmix.stats_created

@app.get("/monthly")
async def ttls_monthly():
    return fuelmix.ttls_monthly[0]
