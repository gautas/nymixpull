"""
testing postgres database tools
"""

from nymixpull.postgres import execute, Table
import pytest

@pytest.fixture
def test_table() -> Table:
    test = Table(name="test", staging=False)
    yield test
    if test.exists:
        test.delete()

def test_execute() -> None:
    assert execute("SELECT COUNT(*) FROM information_schema.tables") is not None

def test_create(test_table) -> None:
    if test_table.exists is True:
        test_table.delete()
    assert test_table.exists is False
    test_table.create()
    assert test_table.exists is True

def test_delete(test_table) -> None:
    if test_table.exists is False:
        test_table.create()
    assert test_table.exists is True
    test_table.delete()
    assert test_table.exists is False