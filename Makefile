.PHONY: up down permissions test update

up:
	docker-compose up --build -d

down:
	docker-compose down

# change ownership of repo files, useful only for my own dev setup
permissions:
	sudo chown -R genpurpuser:genpurpuser .

test:
	poetry run pytest

update:
	poetry run python scripts/update_db.py

api:
	poetry run uvicorn main:app --reload --host 0.0.0.0 --port 8000