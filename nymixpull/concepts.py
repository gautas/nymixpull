"""concepts for nyiso fuel mix data
"""

from dataclasses import dataclass
from typing import NamedTuple, TypeVar, Optional, List
from enum import Enum
# from google.cloud import bigquery
from datetime import datetime


@dataclass
class YearMonth:
    year: int
    month: int

    @property
    def next(self) -> TypeVar("YearMonth"):
        if self.month == 12:
            return YearMonth(
                year=self.year + 1,
                month=1,
            )
        else:
            return YearMonth(
                year=self.year,
                month=self.month + 1,
            )

    @property
    def is_future(self) -> bool:
        return self.year > datetime.now().year or self.month > datetime.now().month


class ParamsDataType(NamedTuple):
    name_postgres: str


class DataType(ParamsDataType, Enum):
    STRING = ParamsDataType(name_postgres="text")
    FLOAT64 = ParamsDataType(name_postgres="real")
    TIMESTAMP = ParamsDataType(name_postgres="timestamp")


class FieldParams(NamedTuple):
    name_orig: str
    datatype: DataType
    datatype_staging: Optional[DataType] = None
    include_staging: Optional[bool] = True
    default: Optional[str] = None


class Field(FieldParams, Enum):
    TIMESTAMP = FieldParams(
        name_orig="Time Stamp",
        datatype=DataType.TIMESTAMP,
        datatype_staging=DataType.STRING,
    )
    TIMEZONE = FieldParams(name_orig="Time Zone", datatype=DataType.STRING)
    FUEL = FieldParams(name_orig="Fuel Category", datatype=DataType.STRING)
    MW = FieldParams(name_orig="Gen MWh", datatype=DataType.FLOAT64)
    CREATED = FieldParams(
        name_orig=None,
        datatype=DataType.TIMESTAMP,
        include_staging=False,
        default="current_timestamp",
    )

    # @property
    # def schemafield(self) -> bigquery.SchemaField:
    #     return bigquery.SchemaField(self.name, self.datatype.name, mode="REQUIRED")

    # @property
    # def schemafield_staging(self) -> bigquery.SchemaField:
    #     return bigquery.SchemaField(
    #         self.name,
    #         self.datatype_staging.name
    #         if self.datatype_staging is not None
    #         else self.datatype.name,
    #         mode="REQUIRED",
    #     )

    @property
    def str_schema_postgres(self, staging: bool = False) -> str:
        return f"{self.name} {(self.datatype if staging is False else self.datatype_staging).name_postgres}{f' DEFAULT {self.default}' if self.default is not None else ''}"


NAMES_FIELDS: List[str] = [field.name for field in Field]
STR_NAMES_FIELDS: str = ", ".join(NAMES_FIELDS)


class FuelParams(NamedTuple):
    name_in_data: str


class Fuel(FuelParams, Enum):
    DUAL = FuelParams(name_in_data="Dual Fuel")
    HYDRO = FuelParams(name_in_data="Hydro")
    NG = FuelParams(name_in_data="Natural Gas")
    NUCLEAR = FuelParams(name_in_data="Nuclear")
    OTHER_FF = FuelParams(name_in_data="Other Fossil Fuels")
    OTHER_RENEW = FuelParams(name_in_data="Other Renewables")
    WIND = FuelParams(name_in_data="Wind")


class FuelGroupParams(NamedTuple):
    fuels: List[Fuel]
    name_in_data: str


class FuelGroup(FuelGroupParams, Enum):
    FOSSIL = FuelGroupParams(
        fuels=[Fuel.DUAL, Fuel.NG, Fuel.OTHER_FF], name_in_data="fossil fuels"
    )
    NUCLEAR = FuelGroupParams(fuels=[Fuel.NUCLEAR], name_in_data="nuclear")
    RENEW = FuelGroupParams(
        fuels=[Fuel.HYDRO, Fuel.WIND, Fuel.OTHER_RENEW], name_in_data="renewable"
    )


def get_fuel_group_name(name_fuel: str) -> str:
    for group in FuelGroup:
        for fuel in group.fuels:
            if name_fuel == fuel.name_in_data:
                return group.name_in_data
