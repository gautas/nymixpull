"""base class for google cloud services
"""

"""the google cloud storage bucket where nyiso mix csv files go 
"""

from dataclasses import dataclass
from typing import Optional
from google.oauth2 import service_account
from functools import cached_property


@dataclass
class Google:
    filepath_cred_gcs: Optional[str] = None

    def set_filepath_cred_gcs(self, filepath_cred_gcs: str) -> None:
        self.filepath_cred_gcs = filepath_cred_gcs

    @cached_property
    def cred(self) -> service_account.Credentials:
        return service_account.Credentials.from_service_account_file(
            self.filepath_cred_gcs
        )

google = Google()