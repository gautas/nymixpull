"""general configuration
"""

from dataclasses import dataclass
from typing import Optional
from .concepts import YearMonth

YEAR_START = 2015
MONTH_START = 12
YEARMONTH_START = YearMonth(year=YEAR_START, month=MONTH_START)


@dataclass
class Config:
    loc: Optional[str] = "."

    def set_loc(self, loc: str) -> None:
        self.loc = loc

    @property
    def loc_zips(self) -> str:
        return f"{self.loc}/zips"


    @property
    def loc_extract(self) -> str:
        return f"{self.loc}/csvs"


config = Config()
