"""the google cloud storage bucket where nyiso mix csv files go 
"""

from dataclasses import dataclass
from typing import Optional
from google.oauth2 import service_account
from google.cloud import storage
from functools import cached_property
from datetime import datetime as dt
from .google import google


@dataclass
class Bucket:
    name: Optional[str] = "nyisomix"

    def set_name(self, name: str) -> None:
        self.name = name

    @cached_property
    def client(self) -> storage.Client:
        return storage.Client(credentials=google.cred)

    @cached_property
    def bucket(self) -> storage.Bucket:
        return self.client.get_bucket(self.name)

    @cached_property
    def loc(self) -> str:
        return dt.now().strftime("%y%m%d")

bucket = Bucket()