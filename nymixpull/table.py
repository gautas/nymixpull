"""tools for working with the bigquery table
"""

from dataclasses import dataclass
from typing import Optional, List
from .google import google
from .concepts import Field, YearMonth
from .config import YEARMONTH_START
from google.cloud import bigquery
from google.cloud.exceptions import Conflict, NotFound
from functools import cached_property
import logging
from datetime import datetime
import pandas as pd


def get_yearmonth(timestamp: str) -> YearMonth:
    pass


@dataclass
class Bigquery:
    @cached_property
    def client(self) -> bigquery.Client:
        return bigquery.Client(credentials=google.cred)

    @cached_property
    def project(self) -> str:
        return self.client.project


@dataclass
class Dataset(Bigquery):
    name: Optional[str] = "nyiso"

    def set_name(self, name: str) -> None:
        self.name = name

    @property
    def id(self) -> str:
        return f"{self.project}.{self.name}"

    @cached_property
    def exists(self) -> bool:
        try:
            self.client.get_dataset(self.id)
        except NotFound:
            return False
        else:
            return True

    def create(self) -> None:
        """following https://googleapis.dev/python/bigquery/latest/usage/datasets.html#creating-a-dataset"""
        dataset = bigquery.Dataset(self.id)
        dataset.location = "US"
        try:
            self.client.create_dataset(dataset, timeout=30)
            logging.info(f"created bigquery dataset {self.id}")
        except Conflict:
            logging.info(f"bigquery dataset {self.id} already exists")


dataset = Dataset()


@dataclass
class Table(Bigquery):
    def __post_init__(self) -> None:
        self.name = self.name_default

    @property
    def name_default(self) -> str:
        pass

    def set_name(self, name: str) -> None:
        self.name = name

    @property
    def id(self) -> str:
        return f"{dataset.id}.{self.name}"

    @cached_property
    def exists(self) -> bool:
        try:
            self.client.get_table(self.id)
        except NotFound:
            return False
        else:
            return True

    @property
    def schema(self) -> List[bigquery.SchemaField]:
        pass

    def create(self) -> None:
        """following https://googleapis.dev/python/bigquery/latest/usage/tables.html#creating-a-table"""
        if dataset.exists is False:
            dataset.create()
        table = bigquery.Table(self.id, self.schema)
        try:
            self.client.create_table(table)
            logging.info(f"created bigquery table {self.id}")
        except Conflict:
            logging.info(f"bigquery table {self.id} already exists")


@dataclass
class Staging(Table):
    @property
    def name_default(self) -> str:
        return "fuelmix_staging"

    @property
    def schema(self) -> List[bigquery.SchemaField]:
        return [
            field.schemafield_staging
            for field in Field
            if field.include_staging is True
        ]

    @property
    def job_config(self) -> bigquery.LoadJobConfig:
        return bigquery.LoadJobConfig(
            write_disposition=bigquery.WriteDisposition.WRITE_APPEND,
            source_format=bigquery.SourceFormat.CSV,
            skip_leading_rows=1,
        )

    def delete(self) -> None:
        try:
            self.client.delete_table(self.id)
            logging.info(f"deleted bigquery table {self.id}")
        except NotFound:
            logging.info(f"could not delete: bigquery table {self.id} not found")


staging = Staging()


@dataclass
class Fuelmix(Table):
    @property
    def name_default(self) -> str:
        return "fuelmix"

    @property
    def schema(self) -> List[bigquery.SchemaField]:
        return [field.schemafield for field in Field]

    @property
    def query_max_timestamp(self) -> str:
        return f"SELECT MAX({Field.TIMESTAMP.name}) AS max_timestamp FROM `{self.id}`"

    @cached_property
    def timestamp_latest(self) -> Optional[datetime]:
        return list(self.client.query(self.query_max_timestamp).result())[0][
            "max_timestamp"
        ]

    @cached_property
    def yearmonth_latest(self) -> YearMonth:
        if self.timestamp_latest is None:
            return YEARMONTH_START
        else:
            return YearMonth(
                year=self.timestamp_latest.year, month=self.timestamp_latest.month
            )

    @cached_property
    def yearmonth_next(self) -> YearMonth:
        return self.yearmonth_latest.next

    @property
    def query_update(self) -> str:
        return f"""
        INSERT `{self.id}` ({Field.TIMESTAMP.name}, {Field.TIMEZONE.name}, {Field.FUEL.name}, {Field.MW.name}, {Field.CREATED.name})
        WITH s AS (
            SELECT
            CASE
            WHEN LENGTH({Field.TIMESTAMP.name}) = 16 THEN PARSE_TIMESTAMP('%m/%d/%Y %H:%M', {Field.TIMESTAMP.name})
            WHEN LENGTH({Field.TIMESTAMP.name}) = 19 THEN PARSE_TIMESTAMP('%m/%d/%Y %H:%M:%S', {Field.TIMESTAMP.name})
            ELSE NULL END AS {Field.TIMESTAMP.name},
            {Field.TIMEZONE.name},
            {Field.FUEL.name},
            {Field.MW.name}
            FROM `{staging.id}`
        )
        SELECT
        {Field.TIMESTAMP.name},
        {Field.TIMEZONE.name},
        {Field.FUEL.name},
        {Field.MW.name},
        CURRENT_TIMESTAMP() AS {Field.CREATED.name}
        FROM
            s
        WHERE
            (({self.query_max_timestamp}) IS NULL) OR {Field.TIMESTAMP.name} > ({self.query_max_timestamp})
        """

    def update(self) -> None:
        logging.info(f"updating {self.id}")
        update_job = self.client.query(self.query_update)
        update_job.result()

    def run_query(self, query: str) -> pd.DataFrame:
        return self.client.query(query).to_dataframe()


fuelmix = Fuelmix()
