"""base classes for NYISO fuel mix data
"""

from dataclasses import dataclass
from datetime import date, datetime
from typing import Optional, List, TypeVar
import requests
import logging
import os
import zipfile
from .config import config
# from .bucket import bucket
# from .table import dataset, staging
from .postgres import staging
from .concepts import YearMonth
from glob import glob
from google.cloud import storage
from functools import cached_property
import time


@dataclass
class NYISOMixDay:
    date: date

    @property
    def str_date(self) -> str:
        return self.date.strftime(format="%Y%m%d")

    @property
    def filename(self) -> str:
        return f"{self.str_date}rtfuelmix.csv"

    @property
    def loc_extract(self) -> str:
        return f"{config.loc_extract}/{self.date.year}_{self.date.month}"

    @property
    def filepath_csv(self) -> str:
        return f"{self.loc_extract}/{self.filename}"
    
    @property
    def filepath_csv_postgres(self) -> str:
        return f"/csvs/{self.date.year}_{self.date.month}/{self.filename}"


    @property
    def name_blob(self) -> str:
        return f"{bucket.loc}/{self.filename}"

    @property
    def blob(self) -> storage.Blob:
        return bucket.bucket.blob(blob_name=self.name_blob)

    @cached_property
    def exists_blob(self) -> bool:
        return self.blob.exists()

    def upload(self) -> None:
        if self.exists_blob:
            logging.info(
                f"blob found in google cloud storage bucket {bucket.name} at {self.name_blob}"
            )
        else:
            logging.info(
                f"uploading {self.filepath_csv} to google cloud storage bucket {bucket.name} at {self.name_blob}"
            )
            self.blob.upload_from_filename(self.filepath_csv)

    # @property
    # def uri(self) -> str:
    #     return f"gs://{bucket.name}/{self.name_blob}"

    # def stage(self) -> None:
    #     logging.info(f"staging {self.uri} to {staging.id}")
    #     load_job = dataset.client.load_table_from_uri(
    #         self.uri, staging.id, job_config=staging.job_config
    #     )
    #     load_job.result()

    def stage(self) -> None:
        staging.add_from_csv(self.filepath_csv_postgres)

@dataclass
class NYISOMixMonth(YearMonth):
    @staticmethod
    def from_yearmonth(yearmonth: YearMonth) -> TypeVar("NYISOMixMonth"):
        return NYISOMixMonth(year=yearmonth.year, month=yearmonth.month)

    @property
    def date_first(self) -> date:
        return date(year=self.year, month=self.month, day=1)

    @property
    def str_date_url(self) -> str:
        return self.date_first.strftime(format="%Y%m%d")

    @property
    def filename_zip(self) -> str:
        return f"{self.str_date_url}rtfuelmix_csv.zip"

    @property
    def url(self) -> str:
        return f"http://mis.nyiso.com/public/csv/rtfuelmix/{self.filename_zip}"


    @property
    def filepath_zip(self) -> str:
        return f"{config.loc_zips}/{self.filename_zip}"

    def download(self) -> None:
        logging.info(f"attempt to download {self.url}")
        if os.path.isfile(self.filepath_zip):
            logging.info(f"file found at {self.filepath_zip}")
        else:
            try:
                with requests.get(self.url) as r:
                    with open(self.filepath_zip, "wb") as f:
                        f.write(r.content)
                logging.info(f"saved to {self.filepath_zip}")
            except:
                logging.exception(20 * "-")
                raise

    @property
    def loc_extract(self) -> str:
        return f"{config.loc_extract}/{self.year}_{self.month}"

    def extract(self) -> None:
        if not os.path.exists(self.loc_extract):
            os.makedirs(self.loc_extract)
        logging.info(f"extracting {self.filepath_zip} to {self.loc_extract}")
        with zipfile.ZipFile(self.filepath_zip, "r") as zip_ref:
            zip_ref.extractall(self.loc_extract)

    @property
    def filepaths_csv(self) -> List[str]:
        return glob(f"{self.loc_extract}/*.csv")

    @property
    def strs_date_csv(self) -> List[str]:
        return [
            filepath.split("/")[-1].split("rtfuelmix.csv")[0]
            for filepath in self.filepaths_csv
        ]

    @property
    def dates_csv(self) -> List[date]:
        return [
            datetime.strptime(str_date, "%Y%m%d").date()
            for str_date in self.strs_date_csv
        ]

    @property
    def nyiso_mix_days(self) -> List[NYISOMixDay]:
        return [NYISOMixDay(date=date_csv) for date_csv in self.dates_csv]

    def upload(self) -> None:
        for day in self.nyiso_mix_days:
            day.upload()
            time.sleep(1)

    def stage(self) -> None:
        for day in self.nyiso_mix_days:
            day.stage()
