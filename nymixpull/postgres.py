"""
tools for working with the postgres database
"""

import psycopg2
import logging
import os
from typing import List, Optional
from dataclasses import dataclass
from .concepts import Field, YearMonth
from .config import YEARMONTH_START
from datetime import date, datetime


def execute(query: str, fetch: bool = True) -> Optional[List]:
    with psycopg2.connect(
        host="db",
        database=os.environ.get("POSTGRES_DB"),
        user=os.environ.get("POSTGRES_USER"),
        password=os.environ.get("POSTGRES_PASSWORD"),
    ) as conn:
        with conn.cursor() as cur:
            logging.info(f"running query {query}")
            cur.execute(query)
            res = cur.fetchall() if fetch else None
    return res


def schema(staging: bool) -> str:
    return ", ".join(
        field.str_schema_postgres
        for field in Field
        if (staging is False or field.include_staging is True)
    )


class Postgres:
    pass


@dataclass
class Table(Postgres):
    name: str
    staging: bool

    @property
    def exists(self) -> bool:
        """
        check if the table exists
        from here: https://database.guide/5-ways-to-check-if-a-table-exists-in-postgresql/
        """
        return execute(
            query=f"""
            SELECT EXISTS (
                SELECT FROM
                    information_schema.tables
                WHERE
                    table_schema LIKE 'public' AND
                    table_type LIKE 'BASE TABLE' AND
                    table_name = '{self.name}'
                ); 
            """
        )[0][0]

    def create(self) -> None:
        """
        create the table
        """
        execute(
            query=f"""
            CREATE TABLE {self.name} ({schema(staging=self.staging)});
        """,
            fetch=False,
        )

    def delete(self) -> None:
        """
        delete the table
        """
        execute(
            query=f"""
            DROP TABLE {self.name};
        """,
            fetch=False,
        )

    def add_from_csv(self, fp: str) -> None:
        execute(
            query=f"""
                COPY {self.name}({','.join(field.name for field in Field if (self.staging is False or field.include_staging is True))})
                FROM '{fp}'
                DELIMITER ','
                CSV HEADER;
                """,
            fetch=False,
        )

    @property
    def query_timestamp_latest(self) -> str:
        return f"SELECT max({Field.TIMESTAMP.name}) FROM {self.name}"

    @property
    def timestamp_latest(self) -> Optional[datetime]:
        return execute(query=f"{self.query_timestamp_latest};")[0][0]

    @property
    def yearmonth_latest(self) -> YearMonth:
        if self.timestamp_latest is None:
            return YEARMONTH_START
        else:
            return YearMonth(
                year=self.timestamp_latest.year, month=self.timestamp_latest.month
            )

    @property
    def yearmonth_next(self) -> YearMonth:
        return self.yearmonth_latest.next

    @property
    def stats_created(self) -> List:
        return execute(f"""
            SELECT
                {Field.CREATED.name},
                COUNT(*)
            FROM
                {self.name}
            GROUP BY
                {Field.CREATED.name}
            ORDER BY
                {Field.CREATED.name} DESC
        """)

    @property
    def ttls_monthly(self) -> List:
        return execute(f"""
            SELECT
                EXTRACT(year FROM {Field.TIMESTAMP.name}) AS YEAR,
                EXTRACT(month FROM {Field.TIMESTAMP.name}) AS MONTH,
                {Field.FUEL.name},
                SUM({Field.MW.name}) AS MW,
                COUNT(*) AS NUM_ROWS
            FROM
                {self.name}
            GROUP BY
                YEAR,
                MONTH,
                {Field.FUEL.name}
        """)


fuelmix = Table(name="fuelmix", staging=False)
staging = Table(name="staging", staging=True)

QUERY_UPDATE = f"""
    INSERT INTO {fuelmix.name} ({Field.TIMESTAMP.name}, {Field.TIMEZONE.name}, {Field.FUEL.name}, {Field.MW.name})
    SELECT 
        {Field.TIMESTAMP.name}, {Field.TIMEZONE.name}, {Field.FUEL.name}, {Field.MW.name}
    FROM
        {staging.name}
    WHERE (({fuelmix.query_timestamp_latest}) IS NULL) OR ({Field.TIMESTAMP.name} > ({fuelmix.query_timestamp_latest}));
"""


def update() -> None:
    """
    update fuelmix with the latest rows in staging
    """
    execute(QUERY_UPDATE, fetch=False)
