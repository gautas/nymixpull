"""
tools for handling common queries on fuelmix
"""

from typing import NamedTuple
from enum import Enum
from .table import fuelmix
import pandas as pd
from functools import cached_property
import logging
from dataclasses import dataclass


@dataclass
class Config:
    loc_result: str = "./"

    def set_loc_result(self, loc_result: str) -> None:
        self.loc_result = loc_result


config = Config()


class QueryParams(NamedTuple):
    query: str


class Query(QueryParams, Enum):
    STATS_UPDATE = QueryParams(
        query="""
            SELECT
                CREATED,
                COUNT(*) AS NUM_ROWS
            FROM
                {table}
            GROUP BY
                CREATED
        """
    )

    MONTHLY_TOTALS = QueryParams(
        query="""
            SELECT
                EXTRACT(year FROM TIMESTAMP) AS YEAR,
                EXTRACT(month FROM TIMESTAMP) AS MONTH,
                FUEL,
                SUM(MW) AS MW,
                COUNT(*) AS NUM_ROWS
            FROM
                {table}
            GROUP BY
                YEAR,
                MONTH,
                FUEL
        """
    )

    @property
    def fp(self) -> str:
        return f"{config.loc_result}{self.name}.csv"

    @cached_property
    def result(self) -> pd.DataFrame:
        logging.info(f"getting result for query {self.name}")
        return fuelmix.run_query(self.query.format(table=fuelmix.id))

    def save(self) -> None:
        logging.info(f"saving query to {self.fp}")
        self.result.to_csv(self.fp, index=False)

    @cached_property
    def read(self) -> pd.DataFrame:
        return pd.read_csv(self.fp)
