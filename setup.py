import setuptools


with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="nymixpull",
    version="0.0.1",
    author="Gautam Sisodia",
    packages=setuptools.find_packages(),
    classifiers=["Progamming Language :: Python :: 3"],
    install_requires=[
        "google-cloud-storage==1.41.1",
        "google-cloud-bigquery==2.22.1",
        "ipython==7.25.0",
        "pandas==1.3.1",
        "pyarrow==5.0.0",
        "altair==4.1.0",
    ],
)
