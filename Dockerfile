FROM python:3.10

# install cron
# https://blog.thesparktree.com/cron-in-docker
# RUN apt-get update && apt-get install -y cron && which cron && rm -rf /etc/cron.*/*

# install curl
# RUN apk --no-cache add curl

# install poetry for dependency management https://python-poetry.org/docs/master/#installation
RUN curl -sSL https://install.python-poetry.org | python3 -

# add location of poetry binary to the path
ENV PATH="/root/.local/bin:${PATH}"

# change to the code directory
WORKDIR /code

# copy over poetry files
COPY poetry.lock pyproject.toml /code/

# install dependencies
RUN poetry install

# now install current project
COPY . /code/
RUN poetry install


# start up cron
# ENTRYPOINT ["/code/entrypoint.sh"]
# CMD ["python3"]